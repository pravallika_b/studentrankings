def getMarksList(fileName) :
    marksList = {}
    for line in open(fileName) :
        marksList[line.split()[0]] = [int(marks) for marks in line.split()[1:]]
    return marksList

def compareMarks(markList1, markList2):
    less = 0
    greater = 0
    for i in range(len(markList1)):
        if markList1[i] < markList2[i]:
            less += 1
        else:
            greater += 1
    if less == len(markList1):
        return ' < '
    elif greater == len(markList1):
        return ' > '
    else:
        return ' # '

def compareAllStudents(marksList):
    result = []
    students = [*marksList]
    for i in range(len(students)):
        for j in range(i + 1, len(students)):
            if (compareMarks(marksList[students[i]], marksList[students[j]]) == ' < '):
                result.append(students[j] + ' > ' + students[i])
            else:
                result.append(students[i] + compareMarks(marksList[students[i]], marksList[students[j]]) + students[j])
    return result
print(compareAllStudents(getMarksList("studentRanking.txt")))
                